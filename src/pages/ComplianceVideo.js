import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';

class ComplianceVideo extends Component {
  render() {
    return (
      <section>
        <div className="header">
          <h2>Compliance Video</h2>
        </div>
        <div className="main-content">
          <div className="video-content">
           <iframe className="" width="854" height="480" src="https://www.youtube.com/embed/TWGYi9VPerk?rel=0&amp;controls=0&amp;showinfo=0"></iframe>
          </div>
        </div>
      </section>
    );
  }
}

export default ComplianceVideo;