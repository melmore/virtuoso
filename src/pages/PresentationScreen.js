import React, { Component } from 'react';
import { Link } from 'react-router';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';

let currentIndex = 1;
let assessmentPage;


class PresentationScreen extends Component {
 constructor(props){
  	super(props);
  	this.state = { 
  		currentSlide: 1
  		};
  }

  render() {
    if (this.props.location.query.page) {
      assessmentPage = (
      <p><Link className="btn btn-secondary" to={"assessment?page="+this.props.location.query.page+"&show="+this.props.location.query.show}><i className="fa fa-list" aria-hidden="true"></i> Take Assessment</Link></p>   
      )
    } else {
      assessmentPage = (
      <p><Link className="btn btn-secondary" to={"assessment?page=1&show="+this.props.location.query.show}><i className="fa fa-list" aria-hidden="true"></i> Take Assessment</Link></p>
      )
    }

    return (
      <section>
        <div className="header">
          <h2>{this.props.location.query.show} Presentation</h2>
          <p>{this.state.currentSlide} of {this.props.location.query.slides} slides</p>
        </div>
        <div className="side-nav">
          <p><Link to="products"><i className="fa fa-home fa-5x" aria-hidden="true"></i></Link></p>
          <p><a className="btn btn-secondary" href={"../presentations/"+this.props.location.query.show +"/"+ this.props.location.query.pptx + ".pptx"}><i className="fa fa-cloud-download" aria-hidden="true"></i> Download Presentation</a></p>
          {assessmentPage}
        </div>
        <div className="main-content presentation-wrap">
           <div className="row">
             <div className="col-md-12">
                <div className="left-arrow pull-left"> 
                <button className="prevBtn circle-dir" onClick={() => this.swapImage(currentIndex-1)}><i className="fa fa-chevron-circle-left fa-5x" aria-hidden="true"></i></button>
                </div>
                    <div className="presentation-slides pull-left">
                      <img id="mainImage" src={"http://virtuoso-bl.4thdown.co/presentations/"+this.props.location.query.show+"/Slide1.jpg"} alt=""/>
                    </div>
                <div className="right-arrow pull-left"> 
                   <button className="nextBtn circle-dir" onClick={() => this.swapImage(currentIndex+1)}><i className="fa fa-chevron-circle-right fa-5x" aria-hidden="true"></i></button>
                </div>
             </div>
           </div>
        </div>
      </section>
    );
  }


 swapImage(imageIndex){

    //NOTE: Set this value to the number of slides you have in the presentation.
    let maxIndex = this.props.location.query.slides;

        //Check if we are at the last image already, return if we are.
        if(imageIndex>maxIndex){
            currentIndex=maxIndex;
            return;
        }

        //Check if we are at the first image already, return if we are.
        if(imageIndex<1){
            currentIndex=1;
            return;
        }

        currentIndex=imageIndex;
        //Otherwise update mainImage
        this.setState( { currentSlide  : currentIndex } );
        document.getElementById("mainImage").src='http://virtuoso-bl.4thdown.co/presentations/'+this.props.location.query.show+'/Slide' +  currentIndex  + '.jpg';
        return;
    }
}

export default PresentationScreen;