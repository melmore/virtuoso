import React, { Component } from 'react';
import { Link } from 'react-router';
/*import ComplianceVideo from './ComplianceVideo.js';*/

import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';

class Layout extends Component {

  render() {
    return (
      <div className="App">
         {this.props.location.pathname !== '/Certificate' &&
        <div className="App-header">
          <h2>Remote Speaker Training</h2>
        </div>
         }
        <nav>
          <Link to="compliance">ComplianceVideo</Link>
          <Link to="products">Products</Link>
        </nav>
        <div className="container">
         {this.props.children}
        </div>
      </div>
    );
  }
}

export default Layout;