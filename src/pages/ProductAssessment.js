import React, { Component } from 'react';
import Request from 'superagent';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';



class ProductAssessment extends Component {
  constructor(props){
  	super(props);
  	this.state = { 
  		assessmentQuestions: [[]],
  		assessmentChoices: [],
  		totalPages: 0,
      user_id: 1,
      first_name: 'Marvin',
      last_name: 'Elmore',
      date: ''

  		};
  }

  componentWillMount(){
  	let page = this.props.location.query.page - 1;


    const url = "https://virtuoso-bl.4thdown.co/api_assessment.php?product="+this.props.location.query.show;
    Request.get(url).then((response)=>{
      //console.log(response);
      this.setState({
      	assessmentQuestions: response.body.question[page],
      	assessmentChoices:response.body.question[page].choice,
      	totalPages: response.body.total
      })
    });
  }

  render() {
    
  	let choices = this.state.assessmentChoices.map((result) => {
        return (
         <li key={result.answer_id}>
           <div className="checkbox">
			  <label><input type="checkbox" value={result.answer_id} /> {result.choice}
			</label>
			</div>
         </li>	
        	)
     });


    return (
      <section>
        <div className="header">
          <h2>Product Assessment</h2>
        </div>
        <div className="main-content assessment">
        <h1>Question {this.props.location.query.page} of {this.state.totalPages}</h1>
           <div className="row">
             <form name="assessmentTest" id="assessmentForm">	
               <div className="col-md-12">
				 <legend>{this.state.assessmentQuestions.id + ". " + this.state.assessmentQuestions.question}</legend>		
					<ul>	
	                  {choices}
	                 </ul>
	               </div>
                <div className="col-md-12 text-center"> 
				    <button name="" onClick={() => this.checkAnswers()} className="btn btn-primary center-block btn-pad">SAVE</button> 
				</div>
			 </form> 
           </div>
        </div>
      </section>
    );
  }


  correctAnswers() {
    let correct = [];

     for(let i = 0; i<this.state.assessmentChoices.length; i++){
        if(this.state.assessmentChoices[i].is_correct === "1"){
        	correct.push(this.state.assessmentChoices[i].answer_id)
        }
     }
      return correct;
  };


  checkAnswers() {
    let correctAnswers =  this.correctAnswers();
    let inputElements=document.getElementById("assessmentForm").elements;   
    let checkedValue=[]; 
       for(var i=0; inputElements[i]; ++i){
       //console.log(inputElements[i]);
       if(inputElements[i].checked){
           checkedValue.push(inputElements[i].value);
       }
    }
     this.checkAssesssmentResults(correctAnswers, checkedValue);
     //return checkedValue;
  };



  checkAssesssmentResults(arr1, arr2) {
    if(arr1.length !== arr2.length){
    	this.props.router.push('/incorrect?page=' + this.props.location.query.page + '&show='+ this.props.location.query.show + '&totalPages=' +this.state.totalPages)
      //Submit Assessment Data
      this.submitAssessmentData(arr2,false);
        return false;
    }
    for(let i = arr1.length; i--;) {
        if(arr1[i] !== arr2[i]){
    	//Submit Assessment Data
      this.submitAssessmentData(arr2,false);
      this.props.router.push('/incorrect?page='+this.props.location.query.page +'&show='+ this.props.location.query.show+ '&totalPages=' +this.state.totalPages)
        return false;
        } 
    }
        this.props.router.push('/correct?page='+this.props.location.query.page +'&show='+ this.props.location.query.show+ '&totalPages=' +this.state.totalPages+ '&userId=' +this.state.user_id)
        //Submit Assessment Data
        this.submitAssessmentData(arr1,true);
        return true;
   }


  submitAssessmentData (submitAnswers, pass) {
    if(pass === true){
        Request.post('https://virtuoso-bl.4thdown.co/post.php')
        .send('{"user_id":"'+this.state.user_id+'", "product":"'+this.props.location.query.show+'","first_name":"'+this.state.first_name+'", "last_name":"'+this.state.last_name+'", "question_id":"'+this.state.assessmentQuestions.id+'", "incorrect":"" , "correct":"'+submitAnswers+'", "date":""}')
        .end(function(err, res) {
        });
    } else {
        Request.post('https://virtuoso-bl.4thdown.co/post.php')
        .send('{"user_id":"'+this.state.user_id+'", "product":"'+this.props.location.query.show+'","first_name":"'+this.state.first_name+'", "last_name":"'+this.state.last_name+'", "question_id":"'+this.state.assessmentQuestions.id+'", "incorrect":"'+submitAnswers+'", "correct":"", "date":""}')
        .end(function(err, res) {
        });
    }
   
  }

 };

export default ProductAssessment;