import React, { Component } from 'react';
//import Request from 'superagent';
import { Link } from 'react-router';
import certificate from '../images/certificate.png';
import crystalens_logo from '../images/Crystalens_sm_logo.png';
import enVista_logo from '../images/enVista_sm_logo.png';
import victus_logo from '../images/Victus_sm_logo.png';
import trulign_logo from '../images/trulign_sm_logo.png';


import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';

class Products extends Component {


   constructor(props){
    super(props);
    this.state = {
      
    };
  }

/*
sortProduct(){
let productAccess = [1,2,3,4];

for(let i=0;i<productAccess.length;i++){
   let access[i] =  productAccess[i];
 } 
}  
*/

render() {

let products = [1,2];
let productList = ['trulignToric', 'crystalensAO', 'enVista', 'victus'];

    return (
      <section>
        <div className="header">
          <h2>Products Page</h2>
        </div>
        <div className="main-content">
             <div className="row">

               {this.props.route.user.crystalensAO === 'true' &&
                <div className="col-md-6">
                  <div className="box">
                    <div className="circle-spot"><img src={crystalens_logo} alt=""/></div>
                    <h1>Crystalens</h1>
                    <p><Link to="presentation/product?show=Crystalens&slides=27&pptx=CrystalensTrulignSpeakerDeck"><strong>Launch Presentation</strong></Link></p>
                    <p><a href="../presentations/Crystalens/CrystalensTrulignSpeakerDeck.pptx"><i className="fa fa-cloud-download" aria-hidden="true"></i> Download Presentation</a></p>
                    <p><Link to="assessment?page=1&show=Crystalens"><i className="fa fa-list" aria-hidden="true"></i> Take Assessment</Link></p>
                  </div>
                </div>
               }
                
                {this.props.route.user.enVista === 'true' &&
                <div className="col-md-6">
                  <div className="box">
                    <div className="circle-spot"><img src={enVista_logo} alt=""/></div>
                    <div className="certificate-badge">
                      <img src={certificate} alt=""/>
                    </div>
                    <h1>enVista</h1>
                    <p><Link to="presentation/product?show=enVista&slides=18&pptx=enVista"><strong>Launch Presentation</strong></Link></p>
                    <p><a href="../presentations/enVista/enVista.pptx"><i className="fa fa-cloud-download" aria-hidden="true"></i> Download Presentation</a></p>
                    <p><Link to="assessment?page=1&show=enVista"><i className="fa fa-list" aria-hidden="true"></i> Take Assessment</Link></p>
                  </div>
                </div>
              }

                {this.props.route.user.trulignToric === 'true' &&
                <div className="col-md-6">
                  <div className="box">
                    <div className="circle-spot"><img src={trulign_logo} alt=""/></div>
                    <h1>Trulign</h1>
                    <p><Link to="presentation/product?show=Trulign&slides="><strong>Launch Presentation</strong></Link></p>
                    <p><a href="../presentations/CrystalensTrulignSpeakerDeck.pptx"><i className="fa fa-cloud-download" aria-hidden="true"></i> Download Presentation</a></p>
                    <p><Link to="assessment?page=1&show=Trulign"><i className="fa fa-list" aria-hidden="true"></i> Take Assessment</Link></p>
                  </div>
                </div>
              }

                {this.props.route.user.victus === 'true' &&
                <div className="col-md-6">
                  <div className="box">
                    <div className="circle-spot"><img src={victus_logo} alt=""/></div>
                    <h1>Victus</h1>
                    <p><Link to="presentation/product?show=Victus&slides="><strong>Launch Presentation</strong></Link></p>
                    <p><a href="../presentations/CrystalensTrulignSpeakerDeck.pptx"><i className="fa fa-cloud-download" aria-hidden="true"></i> Download Presentation</a></p>
                    <p><Link to="assessment?page=1&show=Victus"><i className="fa fa-list" aria-hidden="true"></i> Take Assessment</Link></p>
                  </div>
                </div>
              }


             </div>
        </div>
      </section>
    );
  }
}

export default Products;