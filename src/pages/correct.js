import React, { Component } from 'react';
import Request from 'superagent';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';



class correct extends Component {
  render() {
    return (
      <section>
        <div className="header">
          <h2>Product Assessment</h2>
        </div>

        <div className="main-content incorrect">
          <h1>Feedback - Question {this.props.location.query.page} of {this.props.location.query.totalPages}</h1>
          <p><i className="fa fa-check fa-4x" aria-hidden="true"></i> That is the correct answer. </p>
        </div>
         <div className="col-md-12 text-center"> 
            <button name="" onClick={() => this.moveForward()} className="btn btn-primary center-block btn-pad">NEXT</button> 
        </div>
      </section>
    );
  }

moveForward(){

  let nextpage = parseFloat(this.props.location.query.page) + parseFloat(1);
   
   if(this.props.location.query.page === this.props.location.query.totalPages){
      this.props.router.push('/completion?show='+ this.props.location.query.show);
      //POST completed user
       Request.post('https://virtuoso-bl.4thdown.co/post.php?testCompleted=true')
        .send('{"user_id":"'+this.props.location.query.userId+'","product":"'+this.props.location.query.show+'"}')
        .end(function(err, res) {
        });
   } else {
      this.props.router.push('/assessment?page=' + nextpage + '&show='+ this.props.location.query.show)
   }
  }

}
export default correct;