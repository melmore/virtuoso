import React, { Component } from 'react';
import { Link } from 'react-router';


import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';


class incorrect extends Component {

  render() {
    return (
      <section>
        <div className="header">
          <h2>Product Assessment</h2>
        </div>

        <div className="main-content incorrect">
          <h1>Feedback - Question {this.props.location.query.page} of {this.props.location.query.totalPages}</h1>
          <p><i className="fa fa-times fa-4x" aria-hidden="true"></i> Sorry, that is not the correct answer. </p>
          <p><Link to={"/presentation/product?show=Crystalens&slides=27&pptx=CrystalensTrulignSpeakerDeck&page="+this.props.location.query.page}>Click here to view reference</Link></p>
        </div>
         <div className="col-md-12 text-center"> 
            <button name="" onClick={() => this.backToQuestion()} className="btn btn-primary center-block btn-pad">BACK TO QUESTION</button> 
        </div>
      </section>
    );
  }

backToQuestion(){
 this.props.router.push('/assessment?page=' + this.props.location.query.page + '&show='+ this.props.location.query.show)
}


}
export default incorrect;