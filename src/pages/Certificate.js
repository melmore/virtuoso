import React, { Component } from 'react';
import bausch_lomb from '../images/bausch_lomb_logo.png';
import crystalens_logo from '../images/crystalens_logo.png';
import enVista_logo from '../images/enVista_logo.png';
import trulign_logo from '../images/trulign_logo.png';




import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';



class Certificate extends Component {
    constructor(props){
    super(props);
    this.state = {
      date:0
    };
  }
  
  componentDidMount(){
   this.setDate();
  }

  productLogo(){
    if (this.props.location.query.show === 'Crystalens') {
      return (<img src={crystalens_logo} alt=""/>)
    } 
    if (this.props.location.query.show === 'enVista') {
      return (<img src={enVista_logo} alt=""/>)
    } 
    if (this.props.location.query.show === 'Trulign') {
      return (<img src={trulign_logo} alt=""/>)
    } 
  }

  render() {
    return (
      <section>
        <div className="container">
        <div className="main-content certificate center">
          <div className="content">
          <div className="bausch-lomb-logo">
            <img src={bausch_lomb} alt=""/>
           </div> 

           <h1>Certificate of Recognition</h1>
            <div>This certificate is awarded to</div>
            <div className="certify-user"><h1>{this.props.route.user.certificateName}</h1></div>
            <p>for the successful completion of the<br/>
               Bausch + Lamb Surgical<br/>
               Speaker Alliance On-Demand Training
                  </p>
            
            <div className="product-logo">
            { this.productLogo() } 
           </div>
           
           <div className="container certificate-time">
              <div className="col-md-6">
                <p><strong>Awared on:</strong></p>
                <p className="date">{this.state.date}</p>
              </div>
              <div className="col-md-6">
               <p><strong>Expires on:</strong></p>
               <p className="date">December 31, 2017</p>
              </div>
           </div>

           <div className="certified-signature">
              The certificate is hereby certified by
              <div className="signature-line">
                <hr/>
              </div>
             <div className="product-manager-name">
                <div>Product Manager Name</div>
                <div>Title</div>
             </div>  
           </div>

          </div>

          <div className="trademark-copy">
           &reg; &trade; sdfjkshfksdhfksdfsdjfkhsdkfhksdj
           </div>
        </div>

        </div>
       <button id="certificateBtn" onClick={() => this.getCertificate()} className="btn btn-success center-block btn-pad btn-topspace" href="">Click here to download your certificate</button>
      </section>
    );
  }

  getCertificate() {
  // 
   //document.getElementById("virtuoso-iframe").contentWindow.print();
   //document.getElementById("certificateBtn").style.display = "none";
   window.print();
  }    

  setDate(){
  let today = new Date();
  let dd = today.getDate();
  let yyyy = today.getFullYear();
  let month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    let showMonth = month[today.getMonth()];


  if(dd<10) {
      dd='0'+dd
  } 

  if(showMonth<10) {
      showMonth='0'+showMonth
  } 

  today = showMonth+' '+dd+' '+yyyy;
  this.setState( { date : today } );
} 

}
export default Certificate;