import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../stylesheets/App.css';


class completion extends Component {


  render() {
    return (
      <section>
        <div className="header">
          <h2>Product Assessment</h2>
        </div>

        <div className="main-content incorrect">
          <h1><i className="fa fa-check-circle fa-4x" aria-hidden="true"></i> Completion</h1>
          <p><strong>Congratulation!</strong> You have succesfully completed the asseessment for {this.props.location.query.show} </p>
          <p><button onClick={() => this.props.router.push('/Certificate?show='+ this.props.location.query.show)} className="btn btn-success center-block btn-pad" href="">Click here to download your certificate</button></p>
        </div>
        
         <div className="col-md-12 text-center"> 
            <button name="" onClick={() => this.props.router.push('/products')} className="btn btn-primary center-block btn-pad">BACK TO HOME</button> 
        </div>
      </section>
    );
  }

}
export default completion;