import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import Layout from './pages/Layout';

import Products from './pages/Products';
import ComplianceVideo from './pages/ComplianceVideo';
import PresentationScreen from './pages/PresentationScreen';
import ProductAssessment from './pages/ProductAssessment';
import incorrect from './pages/incorrect';
import correct from './pages/correct';
import completion from './pages/completion';
import Certificate from './pages/Certificate';


import './index.css';

// Parse the URL parameter
let getParameterByName = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\]]/g, "\\$&");
    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

// Give the parameter a variable name
let reap_id = getParameterByName('reap_id');
let user_email = getParameterByName('user_email');
let certificateName = getParameterByName('certificateName');
let trained_products = getParameterByName('trained_products');
let trulignToric = null;
let crystalensAO = null;
let enVista = null;
let victus = null;

/*
?reap_id=1&user_email=speaker@example.com&trained_products=1,2

file:///Users/me/Desktop/4thDown/Application/iframetest.html?reap_id=1&user_email=test@test.com&certificateName=J.%20Kevin%20Belville,%20MD&trained_products=1,2,3
let reap_id = getParameterByName('reap_id');
let user_name = getParameterByName('user_name');
let user_email = getParameterByName('user_email');
let trained_products = getParameterByName('trained_products');
*/

let showProduct = function(){
    
  if(trained_products.indexOf(1) > -1){
    trulignToric = 'true';
  }
  if(trained_products.indexOf(2) > -1){
    crystalensAO = 'true';
  }
  if(trained_products.indexOf(3) > -1){
    enVista = 'true';
  }
  if(trained_products.indexOf(4) > -1){
    victus = 'true';
  }

}


let setCookie = function(){
document.cookie = "complianceVideo=true";
}


let init = function(){
 setCookie()
 showProduct() 
}

init();

const app =  document.getElementById('app');
const userInfo = {
  reap_id: reap_id,
  'user_email':user_email,
	//firstname: 'J.',
  //lastname: 'Belville',
  certificateName:certificateName,
  //Convert trained_products
  enVista: enVista,
  trulignToric: trulignToric,
  crystalensAO:crystalensAO,
  victus:victus
 }

ReactDOM.render(
  <Router history={hashHistory}>	
    <Route path="/" component={Layout}>
      <IndexRoute component={ComplianceVideo}></IndexRoute> 
      <Route path="products" component={Products} user={userInfo}></Route>
      <Route path="Certificate" component={Certificate} user={userInfo}></Route>
      <Route path="compliance" component={ComplianceVideo}></Route>
      <Route path="presentation(/:product)" component={PresentationScreen}></Route>
      <Route path="assessment(/:page)" component={ProductAssessment}></Route>
      <Route path="incorrect" component={incorrect}></Route>
      <Route path="correct" component={correct}></Route>
      <Route path="completion" component={completion}></Route>
    </Route>
  </Router>, app);
